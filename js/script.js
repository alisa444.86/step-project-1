/** =========== Our services - tabs ============= **/

const servicesMenu = $('.our-services-menu');
const servicesMenuLi = $('.our-services-item'); //document.querySelectorAll
const contentListItems = $('.our-services-content-item'); //contentList.querySelectorAll
let prevMenuItem = null;
let prevText = null;

$(servicesMenu).click(showContent);

servicesMenuLi[0].click();

function showContent(event) {
    if (prevMenuItem) {
        $(prevMenuItem).removeClass('active');
    }

    $.each(servicesMenuLi, (i, item) => {
        if ($(item).hasClass('active')) {
            $(item).removeClass('active');
        }
    });

    let activeMenuItem = event.target;
    $(activeMenuItem).addClass('active');
    prevMenuItem = activeMenuItem;

    let dataNumber = $(activeMenuItem).attr('data-number');

    if (prevText) {
        $(prevText).hide();
    }
    $.each(contentListItems, (i, item) => {
        if ($(item).attr('data-number') === dataNumber) {
            $(item).show();
            prevText = item;
        }
    });
}


/**======= Load More Button. Our Amazing Works =========*/

const loadMoreBtn = $('.load-more');
const amazingImg = $('.our-amazing-image');
let i = 12;

for (let x = 0; x < 12; x++) {
    $(amazingImg[x]).addClass('open');
}
for (let j = 12; j < amazingImg.length; j++) {
    $(amazingImg[j]).hide()
}

function loadMore() {
    $('.animation-amazing').removeClass('hidden');
    setTimeout(() => {
        $('.animation-amazing').addClass('hidden');

        if (i === 12) {
            for (i = 12; i < 24; i++) {
                $(amazingImg[i]).addClass('open');
                $(amazingImg[i]).show();
            }
        } else if (i === 24) {
            for (i = 24; i < 36; i++) {
                $(amazingImg[i]).addClass('open');
                $(amazingImg[i]).show();
            }
            $(loadMoreBtn).fadeOut();
        }
    }, 2000);
}

loadMoreBtn.click(loadMore);

/**===== Filter Images ========*/

const filterMenu = $('.our-amazing-menu');
const filterMenuItem = $('.our-amazing-item');

function showFiltered(event) {
    if (event.target.hasAttribute('data-type')) {
        let dataType = $(event.target).attr('data-type');

        $.each(amazingImg, (i, item) => {
            if ($(item).hasClass('open')) {
                if ($(item).attr('data-type') === dataType) {
                    $(item).show();
                } else {
                    $(item).hide();
                }
            }
        })
    } else {
        $.each(amazingImg, (i, item) => {
            if ($(item).hasClass('open')) {
                $(item).show();
            }
            if ($(item[35]).hasClass('open')) {
                $(loadMoreBtn).fadeOut();
            }
        })
    }
}

filterMenu.click(showFiltered);

/**============ Highlight Active Menu Item ===============*/

let prevItem = null;

function addActiveClass(event) {
    if (prevItem) {
        $(prevItem).removeClass('active-item');
    }
    $.each(filterMenuItem, (i, item) => {
        if ($(item).hasClass('active-item')) {
            $(item).removeClass('active-item');
        }
    });
    let activeMenuItem = event.target;

    $(activeMenuItem).addClass('active-item');
    prevItem = activeMenuItem;
}

$(filterMenu).click(addActiveClass);

/**========== Carousel Text Change ==========*/

const reviewItem = $('.review-item');
let prevReview = reviewItem[0];

function showReview() {
    $(prevReview).addClass('hidden');
    $.each(reviewItem, (i, item) => {
        if ($(item).attr('data-number') === $('.slick-current').attr('data-slick-index')) {
            $(item).removeClass('hidden');
            prevReview = item;
        }
    })
}

$('.slick-arrow').click(showReview);

/**===================Gallery==================*/

let grid = $('.gallery-container').masonry({
    itemSelector: '.item-middle, .item-big, .item-small, .item-tiny',
    fitWidth: true,
    columnWidth: 63
});

$(grid).imagesLoaded().progress(function () {
    $(grid).masonry('layout');
});

const hiddenGaleryItems = $('.gallery-item.hidden');
const GalleryLoadBtn = $('.gallery-load-more');

$(GalleryLoadBtn).click(addPictures);

function addPictures() {
    $('.animation').removeClass('hidden');

    setTimeout(() => {
        $('.animation').addClass('hidden');
        $.each(hiddenGaleryItems, (i, item) => {
            $(item).removeClass('hidden');
        });
        $(grid).masonry('appended', hiddenGaleryItems);
        $(GalleryLoadBtn).fadeOut();
    }, 2000);
}


